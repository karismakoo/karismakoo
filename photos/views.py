from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from photos.forms import PhotoForm, PhotoModelForm
from photos.models import Photo
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from photos.forms import PhotoModelForm, CommentModelForm

def toppage(request):
	return HttpResponse('hello')

def single_photo(request, photo_id):
	if int(photo_id) == 0:
		return HttpResponseRedirect(reverse('photo_toppage'))
		
	photo = get_object_or_404(Photo, pk=photo_id)

	if request.method == "GET":
		comment_form = CommentModelForm()
	elif request.method == "POST":
		if not request.user.is_authenticated():
			return HttpResponseRedirect(reverse('login_url'))

		comment_form = CommentModelForm(request.POST)
		if comment_form.is_valid():
			_comment = comment_form.save(commit=False)
			_comment.user = request.user
			_comment.photo = photo
			_comment.save()

			return HttpResponseRedirect(request.path)


	return render(
		request, 'single_photo.html',
		{
			'photo': photo,
			'comment_form': comment_form,
		}
)
@login_required
def edit_photo(request) :
	form = PhotoForm()
	if request.method == 'GET':
		form = PhotoForm()
	elif request.method == 'POST':
		form = PhotoModelForm(request.POST,request.FILES)

		if form.is_valid():
	    	# _photo = Photo(
	    	# 	user=request.user,
	    	# 	image_url=request.POST['image_url'],
	    	# 	description=request.POST['description'],
	    	# )
			_photo = form.save(commit=False)
			_photo.user = request.user
			_photo.save()

			return HttpResponseRedirect('/photos/{0}/'.format(_photo.id))
	
	return render(request, 'edit_photo.html', {
				'form': form,
		})
	