from django.db import models

# Create your models here.
from django.conf import settings

class Profile(models.Model):
	user = models.OneToOneField(settings.AUTH_USER_MODEL)
	gender = models.CharField(max_length=1)
	