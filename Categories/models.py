from django.db import models
from categories.models import CategoryBase

# Create your models here.
class Category(models.Model):  
    about = models.TextField(blank=True)
    parent_cat = models.ForeignKey('self', blank=False, null=True)
    slug = models.SlugField(unique=True, help_text = SLUG_HELP)    
    title = models.CharField(max_length = 26, unique=True)

class Post(models.Model):
	category = models.ForeignKey(Category)